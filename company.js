(function(){

const ERROR = {
  ADMIN_ERROR: 'There is at least one admin',
  PASSWORD_ERROR:"You need rights of administrator",
  TOKEN_ERROR:'incorrect token',
  CREATE_USER:'There is no space to create new user',
  USER_NOT_FIND:'User is not find',
}
const TOKEN = "secret token";
 
  listOfUsers = [];
  
  class Company  {
    constructor(name, maxSize) {
      this.name = name
      this.maxSize = maxSize
    };
  
    #password;
    registerStorage = {
      registerUserCreateCallback : [],
      registerUserUpdateCallba : [],
      registerNoSpaceNotify : [],
    };
    
    users = [];
    curSize = this.users.length;
    adminSize = true;
    userId = 0;
  
    static createSuperAdmin(company) {
      if(!company.adminSize){
        throw ERROR.ADMIN_ERROR
      };;
      const user = new UserAdmin('adName','adLastname',true);
      user.id = Math.floor(Math.random() * 100000);
      company.userId++;
      user.company = company;
      company.adminSize = false;
      company.curSize++;
      return user;
    }
  
    registerUserCreateCallback = function(callback) {		
      this.registerStorage.registerUserCreateCallback.push(callback);
    };
    
    registerUserUpdateCallback = function(callback) {
      this.registerStorage.registerUserUpdateCallback.push(callback);
    };
  
    registerNoSpaceNotifyer = function(callback) {		
      this.registerStorage.registerNoSpaceNotifyer.push(callback);
    };
  
  };
  
  Company.prototype.getUser = function(id) {
    console.log( this.users.filter(item => item.id === id));
  }
  
  class User {
    constructor(name,lastName,isAdmin) {
      this.name = name;
      this.lastName = lastName;
      this.isAdmin = isAdmin;
      this.id = Math.floor(Math.random() * 100000);
    }
  }
  
  class UserAdmin  {
    #password
    #token 
    constructor(name,lastName,isAdmin){
      this.name = name;
      this.lastName = lastName;
      this.isAdmin = isAdmin;
      this.#token = TOKEN;
    }
  
    #checkPassword() {
      let passwordField = prompt('Entere your password','');
      if (!passwordField == '') {
        throw ERROR.PASSWORD_ERROR;
      }
      this.#password = passwordField;
    }
  
    #checkToken() {
      if(this.#token !== TOKEN) {
        throw ERROR.TOKEN_ERROR;
      }
    }
  
    createUser(name,lastName) {
      if(this.company.curSize === this.company.maxSize){
        throw ERROR.CREATE_USER;
      }
      this.#checkPassword();
      this.#checkToken();
      const user = new User(name, lastName, false);
      const message = ERROR.PASSWORD_ERROR;
      this.company.users.push(user);
      this.company.curSize++;
      this.company.registerStorage.registerUserCreateCallback.forEach(callback => {
        if(!user)throw Error(ERROR.USER_NOT_FIND);
        callback(user, this.company.curSize);
      });      
      this.company.registerStorage.registerNoSpaceNotifyer.forEach(callback => {
        callback(message);
      });
      const userProxy = new Proxy (user, {
        set:(o, propperty, value) =>{
          this.company.registerStorage.registerUserUpdateCallback.forEach(callback => {
            o[propperty]  = value;
            if(!user){
              throw Error(ERROR.USER_NOT_FIND);
            }
            callback(user, this.company.curSize);
            
          });
        }
      });
      return userProxy;
    }
    deleteUser(id) {
      this.#checkPassword();
      this.#checkToken();
      let user = this.company.users.filter(item => id  === item.id );
      this.company.users.pop(user);
      this.company.curSize -= 1;
      this.company.registerStorage.registerUserUpdateCallback.forEach(callback => {
        callback(user.id, this.company.curSize);
      })
      const userProxy = new Proxy (user, {
        set:(o, propperty, value) =>{
          this.company.registerStorage.registerUserUpdateCallback.forEach(callback => {
            o[propperty]  = value;
            if(!user){
              throw Error(ERROR.USER_NOT_FIND);
            }
            callback(user.id, this.company.curSize);
          });
        }
      });
    };
  };
  window.Company = Company;
  })()




