const renderCompany = (company) => {
  const tableTemplate = document.querySelector('#table-template');
  const headerTemplate = document.querySelector('#theader');
  const cloneHeader = headerTemplate.content.cloneNode(true);
  const cloneTable = tableTemplate.content.cloneNode(true);
  const logo = cloneHeader.querySelector('.logo');
  const maxSize = cloneHeader.querySelector('#maxSize');
  const curSize = cloneHeader.querySelector('#curSize');


  logo.textContent = company.name;
  maxSize.textContent = company.maxSize;
  curSize.textContent = company.curSize;
  document.body.appendChild(cloneHeader);
  document.body.appendChild(cloneTable);
}

const renderNewUser = (user,curSize) => {
	if (!user.role) {
		user.role = '';
	};
	

	const trow = document.querySelector('#trow');
	const cloneTrow = trow.content.cloneNode(true);
	const tableBody = document.querySelector('tbody')
  const tdList = cloneTrow.querySelectorAll("td");
	const userLine = cloneTrow.querySelector('tr');
	const closeBtn = cloneTrow.querySelector('#close')	
	const userFields = Object.values(user)
	userLine.id = user.id 

	userFields[5] = null

	tdList.forEach((item, i) => {
		item.textContent = userFields[i]
	})


	tdList[5].appendChild(closeBtn)
	tableBody.appendChild(cloneTrow);
	callbackStorage.registerUserCreated.forEach(callback => {
		callback()
	}); 
}

function rerenderUser(userId) {
	const changedUser = document.getElementById(userId.id)
  changedUser.children[1].innerHTML = userId.name
  changedUser.children[2].innerHTML = userId.lastName
  changedUser.children[3].innerHTML = userId.isAdmin
	changedUser.children[4].innerHTML = userId.id
  changedUser.children[5].innerHTML = userId.role
}

function setNumberOfUsers(user,curSize) {
	const tableBody = document.querySelector('tbody')
	const thCollection = tableBody.querySelectorAll('th')
	thCollection.forEach((item,i) => {
		item.textContent =  i + 1
	})
}

function changeCurSize(user,curSize) {
	curSizeEl.textContent =`${curSize}`
}

function unrenderUser(user,id) {
	user.remove(id)
	admin.deleteUser(id)
	callbackStorage.registerUserCreated.forEach(callback => {
		callback()
	}); 
}

const notifications = ( user,massage) => {
	const notifyWindow = document.querySelector('.alert')
	if (!user) { 
		notifyWindow.innerHTML = `${massage}`
		notifyWindow.classList.remove('hide')
		setTimeout(() => { notifyWindow.classList.add('hide') }, 2000)
	return
	}
	notifyWindow.innerHTML = `${massage} ${user}`
	notifyWindow.classList.remove('hide')
	setTimeout(() => { notifyWindow.classList.add('hide') }, 2000)
}
  