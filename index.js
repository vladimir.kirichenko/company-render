const company = new Company('Yojji', 5)
renderCompany(company)

const table = document.querySelector('table')
const curSizeEl =  document.querySelector('#curSize')
curSizeEl.textContent = 1 

let callbackStorage = {
	registerUserCreated:[]
}

registerUserCreated = function(callback) {		
	callbackStorage['registerUserCreated'].push(callback)
}

let admin = Company.createSuperAdmin(company)

table.addEventListener('click', event => {
	let user = event.target.closest("tr")
	let id = user.id
	unrenderUser(user,id)
})

company.registerUserCreateCallback(renderNewUser)
company.registerUserCreateCallback(changeCurSize)
company.registerUserUpdateCallback(changeCurSize) 
company.registerUserUpdateCallback(rerenderUser) 
company.registerUserUpdateCallback(notifications)
company.registerUserCreateCallback(function (user) {
	let message = "Создан новый пользователь:"
	notifications(user.name, message );
  });
registerUserCreated(setNumberOfUsers)
